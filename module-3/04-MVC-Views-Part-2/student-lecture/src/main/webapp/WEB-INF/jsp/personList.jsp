<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<!DOCTYPE html>
<html>
	<head>
		<title>Name List Example</title>
	</head>
	<body>
		<table>
			<tr>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Age</th>
				<th>Adult</th>
			</tr>
			<c:forEach var="person" items="${personList}">
				<tr>
					<td>${person.firstName}</td>
					<td>${person.lastName}</td>
					<td>${person.age}</td>
					<td>${person.adult}</td>
					<!-- for is and get you dont need to put that in there just the varaible name -->
				</tr>
			</c:forEach>
		</table>
		
		<c:out value="${doug.firstName}" /> lives at <c:out value="${doug.address.street}" />
		
		<h1> <c:out value="${param.number }" /></h1>
	
	</body>
</html>
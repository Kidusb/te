<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:set var="pageTitle" value="All Actors List" />

<%@include file="common/header.jspf"%>
<!-- Form goes here -->
<c:url var="formAction" value="/searchResult" />
<form action="${formAction}" method="GET">
	<label for="lastName">Search Actor By Last Name</label>
	<input type="text" placeholder="Search" name="lastName" value="${param.lastName }" />
	<input type="submit" value="Submit" />
</form>

<table class="table">
	<tr>
		<th>Name</th>
	</tr>
	<c:forEach items="${actors}" var="actor">
		<tr>
		<td>${actor.firstName }</td> 
		<td>${actor.lastName } </td>
	
			<!-- What do we print here for each actor? -->
		</tr>
	</c:forEach>
</table>
<%@include file="common/footer.jspf"%>
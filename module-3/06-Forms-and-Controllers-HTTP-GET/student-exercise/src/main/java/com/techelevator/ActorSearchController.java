package com.techelevator;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.techelevator.dao.ActorDao;
import com.techelevator.dao.model.Actor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ActorSearchController {

	@Autowired
	private ActorDao actorDao;

	/* What request mapping do we want here */
	@RequestMapping("/actorSearch")
	public String showSearchActorForm() {
		return "actorList";
//		returns jsp
	}

	/* What about here? */
	@RequestMapping("/searchResult")
	public String searchActors(@RequestParam String lastName, ModelMap map, HttpServletRequest request) {
			/* What arguments go here to get parameters from the page? */
		/* Call the model and pass values to the jsp */
		List<Actor> result = actorDao.getMatchingActors(lastName);
		map.put("actors", result);
		System.out.println(request.getParameter("lastName"));
		return "actorList";
	}
}

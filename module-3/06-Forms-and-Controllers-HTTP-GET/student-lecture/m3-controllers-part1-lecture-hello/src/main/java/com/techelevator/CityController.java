package com.techelevator;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.techelevator.model.city.City;
import com.techelevator.model.city.CityDAO;

@Controller
//Annotation 
public class CityController {
	
	@Autowired
	private CityDAO cityDAO;
	
	@RequestMapping("/citySearch")
	//the next value, the request map has to be unque to the entire project
	public String showCitySearch() {
		//next method has to be a string
		return "city/search";
				// "\WEB-INF\jsp\city\search.jsp" = prefix
					// dont need web-inf/jsp and dont need suffix jsp
	}
	@RequestMapping("/searchResult") 
	public String displaySearchResult(HttpServletRequest request) {
		String countryCode = request.getParameter("countryCode");
		
		List<City> cityList = cityDAO.findCityByCountryCode(countryCode);
		
		request.setAttribute("cities", cityList);
		//attributes can be anything, they are objects 

		return "";
	}

}



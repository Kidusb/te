<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:import url="/WEB-INF/jsp/header.jsp" />


<body>
<c:url var="reviewUrl" value="/pageThree"/>

<form method="POST" action="${reviewUrl}">

    <div>
        What age group are you?<br>
        <input type="radio" name="ageGroup" value="0-19" checked> 0-19<br>
        <input type="radio" name="ageGroup" value="20-39"> 20-39<br>
        <input type="radio" name="ageGroup" value="40-59"> 40-59<br>
        <input type="radio" name="ageGroup" value="60+"> 60+<br>


    </div><br>

    <div>
        <input style="color: white; background-color: blue;" type="submit" value="Next"/>
    </div>

</form>
</body>
</html>
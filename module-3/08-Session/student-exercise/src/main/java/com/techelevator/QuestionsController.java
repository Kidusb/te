package com.techelevator;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;


@Controller
@SessionAttributes("questions")
public class QuestionsController {


    @RequestMapping(path = "/", method = RequestMethod.GET)
    public String showPageOne() {

        return "pageOne";
    }

    @RequestMapping(path = "/pageOne", method = RequestMethod.POST)
    public String handlePageOneForm(
            @RequestParam String favoriteColor, ModelMap model, Questions questions) {


    	questions.setFavoriteColor(favoriteColor);
        model.addAttribute("questions", questions);

        return "redirect:/pageTwo";


    }

    @RequestMapping(path = "/pageTwo", method = RequestMethod.GET)
    public String showPageTwo() {
        return "pageTwo";
    }

    @RequestMapping(path = "/pageTwo", method = RequestMethod.POST)
    public String handlePageOneForm(
            @RequestParam String ageGroup, ModelMap model) {

    	Questions questions = (Questions) model.get("questions");
    	questions.setAgeGroup(ageGroup);
        return "redirect:/pageThree";
    }

    @RequestMapping(path = "/pageThree", method = RequestMethod.GET)
    public String showPageThree() {
        return "pageThree";
    }

    @RequestMapping(path = "/pageThree", method = RequestMethod.POST)
    public String handlePageThreeForm(
            @RequestParam String ageGroup, ModelMap model) {

        Questions questions = (Questions) model.get("questions");
        questions.setAgeGroup(ageGroup);

        return "redirect:/summary";
    }

    @RequestMapping(path = "/summary", method = RequestMethod.GET)
    public String showSummary(ModelMap model) {

        Questions questions = (Questions) model.get("questions");
        return "summary";
    }


}
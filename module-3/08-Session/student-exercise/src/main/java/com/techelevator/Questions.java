package com.techelevator;

public class Questions {

    private String petsName;
    private String favoriteColor;
    private String ageGroup;
	
    
    public String getPetsName() {
		return petsName;
	}
	public void setPetsName(String petsName) {
		this.petsName = petsName;
	}
	public String getFavoriteColor() {
		return favoriteColor;
	}
	public void setFavoriteColor(String favoriteColor) {
		this.favoriteColor = favoriteColor;
	}
	public String getAgeGroup() {
		return ageGroup;
	}
	public void setAgeGroup(String ageGroup) {
		this.ageGroup = ageGroup;
	}


}
package com.techelevator.country;

import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;;


public class JDBCCountryDAO implements CountryDAO{

	private final JdbcTemplate jdbcTemplate;
	
	public JDBCCountryDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public List<Country> findCountryByHeadOfState(String headOfState) {
		List<Country> countries = new ArrayList<>();
		
		String sql = "SELECT code, name FROM country WHERE headofstate = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sql, headOfState);
		
		while(results.next()) {
			Country country = new Country();
			country.setName(results.getString("name"));
			country.setCode(results.getString("code"));
			countries.add(country);
		}
		return countries;
	}

	private Country mapRowToCountry(sqlRowSet results) {
		Country country = new Country();
		country.setName(results.getString("name"));
		country.setCode(results.getString("code"));
		
		while 
		countries.add(mapRowToCountry(results));
	}
	
}

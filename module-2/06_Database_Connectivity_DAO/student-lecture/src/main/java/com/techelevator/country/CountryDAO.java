package com.techelevator.country;

import java.util.List;

public interface CountryDAO {

	List <Country> findCountryByHeadOfState(String headOfState);
	
}

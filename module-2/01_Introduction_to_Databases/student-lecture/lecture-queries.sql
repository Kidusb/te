-- SELECT ... FROM
-- Selecting the names for all countries
SELECT name FROM country;

-- Selecting the name and population of all countries
SELECT name, population FROM country

-- Selecting all columns from the city table
SELECT * FROM country

-- SELECT ... FROM ... WHERE
-- Selecting the cities in Ohio
SELECT * FROM city WHERE district = 'OHIO';

-- Selecting countries that gained independence in the year 1776
SELECT * FROM country WHERE indepyear = 1776;

-- Selecting countries not in Asia
SELECT * FROM country WHERE continent != 'ASIA'
-- can also use: SELECT * FROM country WHERE continent <> 'ASIA'

-- Selecting countries that do not have an independence year
SELECT * FROM country WHERE indepyear IS NULL;
-- Selecting countries that do have an independence year
SELECT * FROM country WHERE indepyear IS NOT NULL;

-- Selecting countries that have a population greater than 5 million
SELECT * FROM country WHERE population > 5000000;


-- SELECT ... FROM ... WHERE ... AND/OR
-- Selecting cities in Ohio and Population greater than 400,000
SELECT * FROM city FROM district = 'OHIO' AND population > 400000;
-- Selecting country names on the continent North America or South America
SELECT * FROM country WHERE continent = 'North America' OR continent = 'South America'

--Using in
SELECT * FROM country WHERE continent IN ('North America', 'South America');

--Using not in
SELECT * FROM country WHERE continent NOT IN ('North America', 'South America');

--Using Like, which is case sensative 
SELECT * FROM country WHERE continent LIKE '%America';

-- Using ILIKE, which is not case sensative and only on postgres
SELECT * FROM country WHERE continent ILIKE '%america';


-- SELECTING DATA w/arithmetic
-- Selecting the population, life expectancy, and population per area
--	note the use of the 'as' keyword

SELECT population, lifeexpectancy, (population/surfacearea) AS density
FROM country
WHERE continent = 'Asia';


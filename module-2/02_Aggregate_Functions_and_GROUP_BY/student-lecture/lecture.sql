-- ORDERING RESULTS

-- Populations of all countries in descending order
SELECT population 
FROM country
ORDER BY pupulation DESC;

--Names of countries and continents in ascending order
SELECT name, continent
FROM country
ORDER BY continent DESC, name DESC;

-- LIMITING RESULTS
-- The name and average life expectancy of the countries with the 10 highest life expectancies.
SELECT name lifeexpectancy 
FROM country
WHERE lifeexpectancy IS NOT NULL
ORDER BY lifeexpectancy DESC
LIMIT 10;

--DISTRICT
--Show only the unique continent names from the counrty table
SELECT DISTINCT continent
FROM country
order by continent; 

--Numeric operators: + - * / %
SELECT round(gnp / population, 4) 
FROM country
WHERE gnp > 0;

-- CONCATENATING OUTPUTS
SELECT 'test' || ' ' || ' concatenation'; 

-- The name & state of all cities in California, Oregon, or Washington.
-- "city, state", sorted by state then city
SELECT ( name || ', ' || district ) as name_and_state
FROM city
WHERE district IN ('California', 'Oregon', 'Washington')
ORDER BY district, name;

-- AGGREGATE FUNCTIONS
-- Average Life Expectancy in the World
SELECT AVG(lifeexpectancy)
FROM country;

-- Total population in Ohio
SELECT population
from city
where district = 'Ohio';

-- The surface area of the smallest country in the world
SELECT MIN(surfacearea)
FROM country;

-- The 10 largest countries in the world
SELECT *
FROM country
ORDER BY surfacearea DESC
LIMIT 10;

SELECT MAX(surfacearea)
FROM country;

-- The number of countries who declared independence in 1991
SELECT COUNT(*) FROM country WHERE indepyear = 1991;

-- GROUP BY

--SELECT name, MIN (surfacearea)
--FROM country
--GROUP BY name

-- Count the number of countries where each language is spoken, ordered from most countries to least
SELECT language, COUNT (countrycode) as countries
FROM countrylanguage 
GROUP BY language, countrycode
ORDER BY countries DESC; 

-- Average life expectancy of each continent ordered from highest to lowest
SELECT continent, AVG (lifeexpectancy) AS avg_lifeexpectancy
FROM country
GROUP BY continent
ORDER BY avg_lifeexpectancy DESC;

-- Exclude Antarctica from consideration for average life expectancy
SELECT CONTINENT, AVG (lifeexpectancy) AS avg_lifeexpectancy
FROM country
WHERE continent != 'Antarctica'
GROUP BY continent
ORDER BY avg_lifeexpectancy DESC;
-- Sum of the population of cities in each state in the USA ordered by state name
SELECT district, SUM(population) as total_population
FROM city
WHERE countrycode = 'USA'
GROUP BY district;

-- The average population of cities in each state in the USA ordered by state name


-- SUBQUERIES

--Find names of cities in europe with gnp > 1,000,000

--SELECT code FROM country WHERE continent = 'Europe' AND gnp > 1000000;

--SELECT * FROM city 
--WHERE countrycode IN ('GBR', 'ITA', 'FRA', 'DEU');

SELECT * FROM city 
WHERE countrycode IN (SELECT code FROM country WHERE continent IN ('Europe', 'Asia') AND gnp > 1000000);

-- Find the names of cities under a given government leader
SELECT *
FROM city
WHERE headofstate IN (SELECT code FROM country WHERE headofstate = 'Elisabeth II');

-- Find the names of cities whose country they belong to has not declared independence yet
SELECT * 
FROM city
WHERE countrycode in (select code from country where indepyear is null)
order by name;

--Find the names of cities with a population > 1,500,000 in countries with a gnp > 1,000,000 and pop > 80,000,000
SELECT name, population
FROM city
WHERE population > 1500000 AND countrycode IN (select code from country where gnp > 1000000 and population > 80000000)
order by population desc
limit 5;



-- Additional samples
-- You may alias column and table names to be more descriptive
SELECT name as city_name
from city as cities;

-- Alias can also be used to create shorthand references, such as "c" for city and "co" for country.
SELECT c.name as city_name, co.name as country_name 
FROM city c, country co;

-- Ordering allows columns to be displayed in ascending order, or descending order (Look at Arlington)
SELECT name, population
FROM city
WHERE countrycode = 'USA' 
ORDER by name ASC, population DESC;

-- Limiting results allows rows to be returned in 'limited' clusters,where LIMIT says how many, and OFFSET (optional) specifies the number of rows to skip
SELECT name, population
FROM city
LIMIT 10 OFFSET 10;

-- Most database platforms provide string functions that can be useful for working with string data. In addition to string functions, string concatenation is also usually supported, which allows us to build complete sentences if necessary.
SELECT (name || ' is in the state of ' || district) AS city_state
FROM city
WHERE countrycode = 'USA';

-- Aggregate functions provide the ability to COUNT, SUM, and AVG, as well as determine MIN and MAX. Only returns a single row of value(s) unless used with GROUP BY.



-- Counts the number of rows in the city table
SELECT count(name)
FROM city;



-- Also counts the number of rows in the city table

SELECT COUNT(population) FROM city;
--or 
SELECT count (*) from city;


-- Gets the SUM of the population field in the city table, as well as
-- the AVG population, and a COUNT of the total number of rows.
SELECT SUM (population), count(*), AVG(population) FROM city;

-- Gets the MIN population and the MAX population from the city table.
SELECT MIN(population) as smallest_city, max (population) as largest_city from city;

-- Using a GROUP BY with aggregate functions allows us to summarize information for a specific column. For instance, we are able to determine the MIN and MAX population for each countrycode in the city table.


--Cast this so we can ROUND
SELECT continent, round (avg(lifeexpectancy), 2) as avg_life
FROM country
WHERE continent != 'Antarctica'
GROUP BY continent
ORDER BY avg_life desc;
-- Write queries to return the following:
-- The following changes are applied to the "dvdstore" database.**

-- 1. Add actors, Hampton Avenue, and Lisa Byway to the actor table.

INSERT INTO actor (first_name, last_name) 
VALUES ('Hampton', 'Avenue');

-- 2. Add "Euclidean PI", "The epic story of Euclid as a pizza delivery boy in
-- ancient Greece", to the film table. The movie was released in 2008 in English.
-- Since its an epic, the run length is 3hrs and 18mins. There are no special
-- features, the film speaks for itself, and doesn't need any gimmicks.
INSERT INTO film (title, description, release_year, language_id, length) 
VALUES ('Euclidean PI', 'The epic story of Euclid as a pizza delivery boy in ancient Greece', 2008, 1, 198);

--SELECT *
--from film
--where title = 'Euclidean PI'
        -- ^ code is to test return

-- 3. Hampton Avenue plays Euclid, while Lisa Byway plays his slightly
-- overprotective mother, in the film, "Euclidean PI". Add them to the film.
start transaction; 

INSERT INTO film_actor (actor_id, film_id) 
values((select actor_id from actor where first_name = 'HAMPTON' and last_name = 'AVANUE'),
(select film_id from film where title = 'Euclidean PI')) 

INSERT INTO film_actor (actor_id, film_id) 
values((select actor_id from actor where first_name = 'LISA' and last_name = 'BAWAY'),
(select film_id from film where title = 'Euclidean PI')) 

ROLLBACK;
COMMIT; 

SELECT *
from film_actor
order by film_id desc

-- 4. Add Mathmagical to the category table.
INSERT INTO category (name) VALUES ('Mathmagical');

SELECT *
FROM film
WHERE title = 'KARATE MOON'

-- 5. Assign the Mathmagical category to the following films, "Euclidean PI",
-- "EGG IGBY", "KARATE MOON", "RANDOM GO", and "YOUNG LANGUAGE"
START TRANSACTION;

INSERT INTO film_category(film_id,category_id) VALUES
((SELECT film_id from film where title = 'Euclidean PI'), (select category_id from category where name = 'Mathmagical'))

INSERT INTO film_category(film_id, category_id) VALUES
((SELECT film_id from film where title = 'EGG IGBY'), (select category_id from category where name = 'Mathmagical'))

INSERT INTO film_category(film_id, category_id) VALUES
((SELECT film_id from film where title = 'KARATE MOON'), (select category_id from category where name = 'Mathmagical'))

INSERT INTO film_category(film_id, category_id) VALUES
((SELECT film_id from film where title = 'RANDOM GO'), (select category_id from category where name = 'Mathmagical'))

INSERT INTO film_category(film_id, category_id) VALUES
((SELECT film_id from film where title = 'YOUNG LANGUAGE'), (select category_id from category where name = 'Mathmagical'))

commit; 
rollback;

-- 6. Mathmagical films always have a "G" rating, adjust all Mathmagical films
-- accordingly.
-- (5 rows affected)
UPDATE film set rating = 'G'
where film_id in (select film_id from film_category where category_id = (select category_id from category where name = 'Mathmagical'))

-- 7. Add a copy of "Euclidean PI" to all the stores.
start transaction; 

insert into inventory(film_id, store_id)
values ((select film_id from film where title = 'Euclidean PI'),1)

insert into inventory(film_id, store_id)
values ((select film_id from film where title = 'Euclidean PI'),2)
commit;
rollback;
-- 8. The Feds have stepped in and have impounded all copies of the pirated film,
-- "Euclidean PI". The film has been seized from all stores, and needs to be
-- deleted from the film table. Delete "Euclidean PI" from the film table.
-- (Did it succeed? Why?)
DELETE FROM film where title = 'Euclidean PI'
-- <YOUR ANSWER HERE>
        --yes it worked, not being used by others 
-- 9. Delete Mathmagical from the category table.
-- (Did it succeed? Why?)
-- <YOUR ANSWER HERE>
DELETE FROM category where name = 'Mathmagical'
-- no it did not work, it is being used by another 


-- 10. Delete all links to Mathmagical in the film_category tale.
-- (Did it succeed? Why?)
-- <YOUR ANSWER HERE>

delete from film_category where category_id = 18
--yes worked because it is a primary key 

-- 11. Retry deleting Mathmagical from the category table, followed by retrying
-- to delete "Euclidean PI".
-- (Did either deletes succeed? Why?)
-- <YOUR ANSWER HERE>

DELETE FROM category where name = 'Mathmagical'

delete from film where title = 'Euclidean PI'
-- first delete worked but the second delete did not work since there is no longer an euclidean pi

-- 12. Check database metadata to determine all constraints of the film id, and
-- describe any remaining adjustments needed before the film "Euclidean PI" can
-- be removed from the film table.

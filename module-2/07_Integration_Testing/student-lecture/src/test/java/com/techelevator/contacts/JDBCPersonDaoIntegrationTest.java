package com.techelevator.contacts;

import static org.junit.Assert.assertEquals;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.junit.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import com.techelevator.contacts.Person;
import com.techelevator.contacts.PersonDao;

public class JDBCPersonDaoIntegrationTest {
	
	private static SingleConnectionDataSource dataSource;
//need data connection
	private PersonDao dao;
//create class, call it dao because there should only be 1 dao file

	@BeforeClass
	//this makes data base connection
	public static void setupDataSource() {
		dataSource = new SingleConnectionDataSource();
		dataSource.setUrl("jdbc:postgresql://localhost:5432/contacts");
		dataSource.setUsername("postgres");
		dataSource.setPassword("postgres1");
		dataSource.setAutoCommit(false);
		//this has to be set to false becuase if not it will commit all changes 
	}
	
	@AfterClass
	//needed to destroy the data source
	public static void closeDataSource() throws SQLException {
		dataSource.destroy();
	}
	
	@After
	public void rollback() throws SQLException {
		dataSource.getConnection().rollback();
	}
	
	@Before
	public void setup() {
		dao = new JdbcPersonDao(dataSource);
	}

	@Test 
	public void select_person_by_id() {
		Person person = getPerson();
				
		dao.save(person); // insert the person so we can select it for out test
		
		//Act 
		Person actualPerson = dao.getById(person.getPersonId() );
		Assert.assertNotNull("Person returned null", actualPerson);
		Assert.assertEquals("Actual person does not equal expected person", person, actualPerson);

				//cant find person so we have to create one
	}
	
	@Test //testing insert
	public void insert_person() {
		//arrange
		Person person = getPerson();
		//act
		dao.save(person);
		//assert
		Assert.assertNotEquals(0, person.getPersonId() );
		Person actualPerson = dao.getById(person.getPersonId() );
		
		Assert.assertNotNull("Person returns null", actualPerson);
		Assert.assertEquals("Actual person does not equal expected person", person, actualPerson);

	}
	
	@Test //UPDATE test
	public void update_person() {
		//arrange
		Person person = getPerson();
		dao.save(person); //insert the person
		person.setFirstName("UpdatedFirstName");
		person.setLastName("UpdatedLastName");
		person.setDateOfBirth(LocalDate.parse("1980-10-24", DateTimeFormatter.ofPattern("yyyy-MM-dd")) );
		//act
		dao.update(person);
		//assert
		Person updated = dao.getById(person.getPersonId() ); //get the person values from database
		Assert.assertNotNull(updated);
		Assert.assertEquals(person, updated);
	}
	
	@Test //delete test
	public void delete_person() {
		//Arrange
		Person person = getPerson(); 
		dao.save(person);
		//act
		dao.delete(person.getPersonId() );
		//assert
		Person actual = dao.getById(person.getPersonId() );
		Assert.assertNull(actual);
	}
	
	
	private Person getPerson() {
		Person person = new Person();
		person.setFirstName("testFirstName");
		person.setLastName("testLastName");
		person.setDateOfBirth(LocalDate.now() );
		return person;
	}
}

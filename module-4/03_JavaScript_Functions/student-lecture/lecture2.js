function greeting() {
    console.log("Hello");
}

/* Spread Opreator */
function multipleWithSpread(x, y, z) {
    return x * y * z;
}

let args = [1, 2, 3];
console.log(multipleWithSpread(...args));

function addWithSpread(a, b, c) {
    return a + b + c;
}

let str = "Tech Elevator";
console.log(addWithSpread(...str));
package com.techelevator;

import org.junit.*;

public class StringBitsTest {

	private StringBits stringBits;
	
	@Before
	public void setup() {
		stringBits = new StringBits();
	}
	
	@Test
	public void test_if_return_normal() {
		String result = stringBits.getBits("Hello");
		Assert.assertEquals("Hlo", result);
	}
	@Test
	public void test_if_string_is_2_characters() {
		String result = stringBits.getBits("Hi");
		Assert.assertEquals("H", result);
	}
	@Test
	public void test_if_string_is_no_characters() {
		String result = stringBits.getBits("");
		Assert.assertEquals("", result);
	}
	@Test
	public void if_string_is_a_number() {
		String result = stringBits.getBits("1");
				Assert.assertEquals("1", result);
	}
	@Test
	public void if_string_is_long() {
		String result = stringBits.getBits("Heeololeo");
		Assert.assertEquals("Hello", result);
	}
	@Test
	public void if_string_is_longer() {
		String result = stringBits.getBits("Brotherhintsingso");
		Assert.assertEquals("Bohritigo", result);
	}
	
}


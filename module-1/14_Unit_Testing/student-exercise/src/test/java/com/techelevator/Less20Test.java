package com.techelevator;

import static org.junit.Assert.fail;
import org.junit.*;

public class Less20Test {
	// first create a file in test/java 
	//create a variable for calss testing 
	private Less20 less20;
	//Create a for method that istantiates the class being tested & assigns it to variable in step 2
	//write test for the requirments or conditions
	
	@Before
	public void setup() { 
	less20 = new Less20();
	}
	//Write the test
	
	@Test
	public void returns_true_for_38() {
		//arrange (often optional)
		
		//act
		boolean result = less20.isLessThanMultipleOf20(38);
		//assert
		Assert.assertTrue(result);
		//not done yet, have to test other numbers 39,41,42,0,-#, etc.
		// tests that cover all things you can think of or covers edges  
	}
	
	@Test
	public void returns_false_if_negative() {
		boolean result = less20.isLessThanMultipleOf20(-20);
				Assert.assertFalse(result);
	}
	@Test
	public void returns_true_for_39() {
		boolean result = less20.isLessThanMultipleOf20(39);
		Assert.assertTrue(result);
	}
	@Test
	public void returns_true_for_19() {
		boolean result = less20.isLessThanMultipleOf20(19);
		Assert.assertTrue(result);
	}
	@Test
	public void returns_true_for_18() {
		boolean result = less20.isLessThanMultipleOf20(18);
		Assert.assertTrue(result);
	}
	@Test
	public void returns_false_for_20() {
		boolean result = less20.isLessThanMultipleOf20(20);
				Assert.assertFalse(result);
	}
	@Test
	public void returns_false_for_41() {
		boolean result = less20.isLessThanMultipleOf20(41);
				Assert.assertFalse(result);
	}
	@Test
	public void returns_false_for_42() {
		boolean result = less20.isLessThanMultipleOf20(42);
				Assert.assertFalse(result);
	}
//	@Test
//	public void returns_false_if_no_number() {
//		boolean results = less20.isLessThanMultipleOf20();
//		if(results.equals(0));
//		Assert.fail("Strings not allowed.");
//		}

}
	


package com.techelevator;

import org.junit.*;

public class AnimalGroupNameTest {
	
	private AnimalGroupName animalGroupName;
	
	@Before
	public void setup() {
		animalGroupName = new AnimalGroupName();
	}
	@Test 
	public void if_empty_text() {
		Assert.assertEquals("unknown", animalGroupName.getHerd("") );
		}
//	@Test
//	public void test_upper_lower_case() {
//		String getHerd(String animalName) = new String getHerd(String animalName);
//		}
	
	@Test
	public void test_animal_group_names() {
//		Map<String, String> animals = new HashMap<> ();
		 Assert.assertEquals("Crash", animalGroupName.getHerd("rhino") );
		 Assert.assertEquals("Crash", animalGroupName.getHerd("RHINO") );
		 Assert.assertEquals("Tower", animalGroupName.getHerd("giraffe") );
		 Assert.assertEquals("Tower", animalGroupName.getHerd("GIRAFFE") );
		 Assert.assertEquals("Herd", animalGroupName.getHerd("elephant") );
		 Assert.assertEquals("Herd", animalGroupName.getHerd("ELEPHANT") );
		 Assert.assertEquals("Murder", animalGroupName.getHerd("crow") );
		 Assert.assertEquals("Murder", animalGroupName.getHerd("CROW") );
		 Assert.assertEquals("Kit", animalGroupName.getHerd("pigeon") );
		 Assert.assertEquals("Kit", animalGroupName.getHerd("PIGEON") );
		 Assert.assertEquals("Pat", animalGroupName.getHerd("flamingo") );
		 Assert.assertEquals("Pat", animalGroupName.getHerd("FLAMINGO") );
		 Assert.assertEquals("Herd", animalGroupName.getHerd("deer") );
		 Assert.assertEquals("Herd", animalGroupName.getHerd("DEER") );
		 Assert.assertEquals("Pack", animalGroupName.getHerd("dog") );
		 Assert.assertEquals("Pack", animalGroupName.getHerd("DOG") );
		 Assert.assertEquals("Float", animalGroupName.getHerd("crocodile") );
		 Assert.assertEquals("Float", animalGroupName.getHerd("CROCODILE") );
		 Assert.assertEquals("unknown", animalGroupName.getHerd("Zebra") );
		 Assert.assertEquals("unknown", animalGroupName.getHerd("ZEBRA") );




		
	        
//			Map<String, String> animalNames = animalGroupName.animalGroupName(animals);
			
					
					//lecture.robPeterToPayPaul(peterPaul);

	        
	        
//			Assert.assertEquals(new String ("Crash"), animals.get("rhino"));

//		String result = animalGroupName.getHerd("rhino");
//		Assert.assertEquals("rhino", result);
//			
	        }
	
}



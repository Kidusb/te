package com.techelevator;

import org.junit.*;

public class SameFirstLastTest {

	private SameFirstLast sameFirstLast;
	
	@Before
	public void setup() {
		sameFirstLast = new SameFirstLast();
	}
	
	@Test
	public void true_if_more_than_1_and_first_last_not_equal() {
		int[] nums = {1,2,3};
		boolean result = sameFirstLast.isItTheSame(nums);
		Assert.assertFalse(result);
	}
	@Test
	public void true_if_more_than_1_and_first_last_equal() {
		int[] nums = {1,2,3,1};
		boolean result = sameFirstLast.isItTheSame(nums);
		Assert.assertTrue(result);
	}
	@Test
	public void true_if_more_less_than_1() {
		int[] nums = {};
		boolean result = sameFirstLast.isItTheSame(nums);
		Assert.assertFalse(result);
	}
	@Test
	public void true_if_more_than_2_and_first_last_equal() {
		int[] nums = {1,2,1};
		boolean result = sameFirstLast.isItTheSame(nums);
		Assert.assertTrue(result);
	}

}


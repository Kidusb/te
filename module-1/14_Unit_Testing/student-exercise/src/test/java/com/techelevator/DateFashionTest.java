package com.techelevator;

import org.junit.*;

public class DateFashionTest {

	private DateFashion dateFashion;
	
	@Before 
	
	public void setup() {
		dateFashion = new DateFashion();
	}
	
	// if either have 8 or more stylish return yes
	// if either have 2 or less stylish return no
	// otherwise maybe
	
	@Test
	public void if_either_stylish_is_8_or_more_return_yes() {
		int table = dateFashion.getATable(5, 10);
		Assert.assertEquals(5, table);
	}
	
	@Test
	public void if_either_stylish_is_5_or_more_return_yes() {
		int table = dateFashion.getATable(5, 5);
		Assert.assertEquals(5, table);
	}
	
	@Test
	public void if_either_stylish_is_2_or_less_return_no() {
		int table = dateFashion.getATable(5, 2);
		Assert.assertEquals(5, table);
	}
	
	@Test
	public void if_either_stylish_is_0_or_less_return_no() {
		int table = dateFashion.getATable(0, 5);
		Assert.assertEquals(table, table);
	}
	
}

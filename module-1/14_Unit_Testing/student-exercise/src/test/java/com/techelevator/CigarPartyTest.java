package com.techelevator;

import org.junit.*;

public class CigarPartyTest {

	private CigarParty cigarParty;
	
	@Before
	public void setup() {
		cigarParty = new CigarParty();
	}
	
	// Cigars true if between 60 - 40
	// cigars false above 60
	// false below 40
	// false if negative 
	
	@Test
	public void cigars_are_true_if_40_or_60() {
		boolean result = cigarParty.haveParty(40, false);
				 Assert.assertEquals(true, result);
		result = cigarParty.haveParty(60, false);
				 Assert.assertEquals(true, result);
	}
	@Test
	public void cigars_are_true_if_between_100_and_weekend() {
		boolean result = cigarParty.haveParty(100, true);
				 Assert.assertEquals(true, result);
	}
	@Test
	public void party_not_successful_weekend_under_40() {
		boolean result = cigarParty.haveParty(39, true);
				 Assert.assertEquals(false, result);
	}
	@Test
	public void party_not_successful_not_weekend_and_under_40() {
		boolean result = cigarParty.haveParty(39, false);
				 Assert.assertEquals(false, result);
	}
	@Test
	public void if_success_and_number_of_cigars_are_correct() {
		boolean result = cigarParty.haveParty(60, false);
			Assert.assertFalse(result);
	}
	
		
//		public boolean haveParty(int cigars, boolean isWeekend) {
//		        int minimumCigarCount = 40;
//		        int maximumCigarCount = 60;
//
//		        boolean hasMinimumCigars = cigars >= minimumCigarCount;
//		        boolean withinMaxRangeOfCigars = (!isWeekend && cigars <= maximumCigarCount) || isWeekend;
//		        boolean successful = hasMinimumCigars && withinMaxRangeOfCigars;
//		 }		
//		boolean success = cigarParty.haveParty(40, s);
//		assert.assertEquals (success);
		
		
		
	
}

package com.techelevator;

import org.junit.*;

public class NonStartTest {
	
	private NonStart nonStart;
	
	@Before
	public void setup() {
		nonStart = new NonStart();
		
	}
	
	@Test
	public void test_return_correctly() {
		String result = nonStart.getPartialString("Hello", "There");
		Assert.assertEquals("ellohere", result);
	}
	@Test
	public void test_return_correctly2() {
		String result = nonStart.getPartialString("java", "code");
		Assert.assertEquals("avaode", result);
	}
	@Test
	public void test_return_correctly_if_2_letters() {
		String result = nonStart.getPartialString("Lo", "Re");
		Assert.assertEquals("oe", result);
	}

}

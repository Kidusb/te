package com.techelevator;

import org.junit.*;

public class FrontTimesTest {

	private FrontTimes frontTimes;
	
	@Before
	public void setup() {
	frontTimes = new FrontTimes();
	}
	
	//Negative integers should not be accepted
	// return the first 3 characters
	// if less than 3 return whatever is there
	
	@Test
	public void check_if_string_retuns_correct() {
		String result = frontTimes.generateString("Chocolate", 3 );
		Assert.assertEquals("ChoChoCho", result);
		
	    }

	@Test
	public void check_if_string_retuns_long_letters_correct() {
		String result = frontTimes.generateString("Chocolate", 2 );
		Assert.assertEquals("ChoCho", result);
	}
	@Test
	public void check_if_string_retuns_3_letters_correct() {
		String result = frontTimes.generateString("Abc", 3 );
		Assert.assertEquals("AbcAbcAbc", result);
	}
	
	@Test
	public void check_if_string_retuns_short_letters_correct() {
		String result = frontTimes.generateString("Ol", 3 );
		Assert.assertEquals("OlOlOl", result);
		
	}
	@Test
	public void check_if_string_retuns_single_letters_correct() {
		String result = frontTimes.generateString("X", 5 );
		Assert.assertEquals("XXXXX", result);
	}
	@Test
	public void check_if_string_returns_with_no_letters() {
		String result = frontTimes.generateString("", 5 );
		Assert.assertEquals("", result);
	}
	
}



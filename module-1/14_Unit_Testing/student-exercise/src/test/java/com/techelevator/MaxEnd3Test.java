package com.techelevator;

import java.util.List;

import org.junit.*;

public class MaxEnd3Test {

	private MaxEnd3 maxEnd3;
	
	@Before
	public void setup() {
		maxEnd3 = new MaxEnd3();
	}
	
	@Test
	public void check_if_it_will_return_3() {
		int[] nums = {1,2,3};
		int[] result = maxEnd3.makeArray(nums);
		Assert.assertEquals(3, 3);
	}
	@Test
	public void check_if_it_will_return_largest() {
		int[] nums = {11,9,5};
		int[] result = maxEnd3.makeArray(nums);
		Assert.assertEquals(11, 11);
	}
	
	@Test
	public void check_if_it_will_return_0() {
		int[] nums = {0,0,0};
		int[] result = maxEnd3.makeArray(nums);
		Assert.assertEquals(0, 0);
	}
	
}

package com.techelevator;

import org.junit.*;

public class Lucky13Test {
	
	private Lucky13 lucky13; 

	@Before
	public void setup() {
	lucky13 = new Lucky13();
	}
	
	//Return true if there are no 1s and no 3s
	//Return false if there are 1s or 3s

	@Test
	public void if_there_are_1s_or_3s_return_false() {
		int[] nums = {1, 3};
		boolean result = lucky13.getLucky(nums);
		Assert.assertFalse(result);
	}

	@Test
	public void if_there_are_no_1s_or_3s_return_true() {
		int[] nums = {2, 4};
		boolean result = lucky13.getLucky(nums);
		Assert.assertTrue(result);
	}
//	@Test
//	public void if_there_are_1s_and_3s_return_false() {
//	int[] nums = {5, 7};
//    for (int i = 0; i < nums.length; i++) {
//    	if(nums[i] == 1 || nums[i] == 3);
//    }
//	boolean result = lucky13.getLucky(nums);
// 
//	Assert.assertFalse(result);
//	
//	}
//	
}



package com.techelevator.trees;

import org.junit.*;

public class FruitTreeTest {
	//First thing to do is declare a private variable for fruit tree
	
	private FruitTree fruitTree;
	
	@Before
	public void setup() {
		fruitTree = new FruitTree("Pear", 10);
		//fruit tree has constructors, we added pear and 10
		//has getters and setters, don't need to write test for all getters/setters
	}
	
	//write simple true test for fruit can pick up
	@Test
	public void pick_2_fruit() {
		//Arrange
				// none because already set
		//Act
		boolean success = fruitTree.pickFruit(2);
		//Assert
		Assert.assertTrue(success);
		//not only thing changed about tree, now we removed 2 trees 
		Assert.assertEquals(8, fruitTree.getPiecesOfFruitLeft() );
	}

	//write for pick half and pick too many
	@Test
	public void pick_half_then_pick_too_many () {
		
		//arrange
		
		fruitTree.pickFruit(5);
		
		//act
		
		boolean success = fruitTree.pickFruit(6);
		
		//assert
		
		Assert.assertFalse(success);	
		Assert.assertEquals(5, fruitTree.getPiecesOfFruitLeft() );
		
	}
	@Test
	public void correct_type_of_fruit() {
		Assert.assertEquals("Pear", fruitTree.getTypeOfFruit());
	}
	
	@Test
	public void cannot_pick_negative_fruit () {
		fruitTree.pickFruit(-5);
		Assert.assertEquals(10, fruitTree.getPiecesOfFruitLeft());
		
	}
	
}

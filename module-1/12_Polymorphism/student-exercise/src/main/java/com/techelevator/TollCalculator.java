package com.techelevator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TollCalculator {

	public static void main(String[] args) {
		
		List<Vehicle> vehicles = new ArrayList<>();
		Map<Vehicle,Integer> distance = new HashMap<>();
		
		vehicles.add(new Car(false));
		vehicles.add(new Car(true));
		vehicles.add(new Tank());
		vehicles.add(new Truck(4));
		vehicles.add(new Truck(6));
		vehicles.add(new Truck(8));

		distance.put(vehicles.get(0), 100);
		distance.put(vehicles.get(1), 75);
		distance.put(vehicles.get(2), 240);
		distance.put(vehicles.get(3), 150);
		distance.put(vehicles.get(4), 101);
		distance.put(vehicles.get(5), 180);
		
		int totalDistance = 0;
		double totalToll = 0.00;
		
		System.out.println("```");
		System.out.println("Vehicle\t\t\tDistance Travled\t Toll $");
		System.out.println("-------------------------------------------------------");
			for (Vehicle vehicle : vehicles) {
				int travled = distance.get(vehicle);
				double toll = vehicle.calculateToll(travled);
				totalDistance += travled;
				totalToll += toll;
				
				String whatIsIt = String.format("%-18s\t%d\t\t\t$%.2f", vehicle, travled, toll);
				System.out.println(whatIsIt);
			}
		System.out.println("\nTotal Miles Traveled: " + totalDistance);
					// \n lets me creater space 
		System.out.format("Total Tollbooth Revanue: $" + totalToll);
		System.out.println("\n```");
	}

}

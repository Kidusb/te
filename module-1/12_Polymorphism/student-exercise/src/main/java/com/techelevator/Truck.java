package com.techelevator;

public class Truck implements Vehicle{
	int numberOfAxles;
		//getters 
	public Truck(int numberOfAxles) {
		this.numberOfAxles = numberOfAxles;
	} 
	
	public int getNumberOfAxles() {
		return numberOfAxles;
	}
	
	@Override 
	public double calculateToll(int distance) {
		if(numberOfAxles <= 4) {
			return distance * 0.040;
		}
		if(numberOfAxles <= 6) {
			return distance * 0.045;
		}
		return distance * 0.048;
	}
	
 	@Override 
	public String getType() {
		return "Tank";
 	}
		
	public String toString() {
		return getType() + " (" + numberOfAxles + " axles)";
	}
}

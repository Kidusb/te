package com.techelevator.farm;

public interface Singable {

	// Methods/ behaviors will tell us what is singable and whats not
	
	String getSound(); 
	// no getSound() { because no block of code is written inside interface
	
	String getName();
	// no need to state its public because everything in interface is public
	
	
	
	}

package com.techelevator.paint;

public class LatexPaint implements Paint{

	private int costPerBucket = 20;
	
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "Latex Paint";
	}

	@Override
	public double calculateCost(int area) {
		// TODO Auto-generated method stub
		return (area/600) * costPerBucket;
	}

}

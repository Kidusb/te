package com.techelevator;

public class Rectangle {
	
	private int height = 0;
	private int width = 0;
	
	public Rectangle() {
		//^ defult constructor, no argument
	}
	
	public Rectangle(int width, int height) {
		this.height = height;
		this.width = width;
	}
	
	public int getHeight () {
		return this.height;
	}
		//^ getter
	
	public void setHeight(int height) {
		this.height = Math.abs(height);
		//^ setter
	}
	
	public int getWidth () {
		return this.width;
	}
		//^ getter
	
	public void setWidth(int width) {
		this.width = Math.abs(width);
		//^ setter
	}
	
	//Derived properties - calculate values each tune using number variables
	public int getArea() {
		return this.height * this.width;
	}
	
	//Member Method/ functions provide behavior
	public boolean isLargerThan(Rectangle other) {
		return this.getArea() > other.getArea();
	}
	
	public boolean isLargerThan (int width, int height) {
		//method overloading ^ same signiture with either dif number of arguments or arguments at dif data type
		return this.getArea() > (width*height) ;
 		}
	
	private int getValidDimensions(int i) {
		if (i<0) {
			return Math.abs(i);
		}
			return i;
		}
	@Override
	public boolean equals(Object obj) {
		// must be casted into rectangle
		Rectangle other = (Rectangle) obj;
		return this.height == other.getHeight() && this.width == other.getWidth ();
	}
	@Override
	public String toString() {
		return this.width + "x" + this.height + " - " + this.getArea() + 
 	}
	}


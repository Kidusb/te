package com.techelevator;

import org.junit.*;
import static org.junit.Assert.*;

public class KataFizzBuzzTest {
	
	KataFizzBuzz fizzBuzzTest;
	
	@Before 
	public void setup() {
		fizzBuzzTest = new KataFizzBuzz();
	
	}
	
//	* If the number is divisible by 3, convert the number to the string, "Fizz".
//	* If the number is divisible by 5, convert the number to the string, "Buzz".
//	* If the number is divisible by 3 AND 5, convert the number to the string, "FizzBuzz"
//	* For all other numbers between 1 and 100 (inclusive), simply convert the number to a string.
//	* Any number that is not between 1 and 100 (inclusive), convert the number to an empty string.

	@Test 
	public void return_fizz_if_number_is_divisible_by_3 () {
		String result = fizzBuzzTest.fizzBuzz(3);
		Assert.assertEquals("Fizz", result);
	}
	@Test 
	public void return_buzz_if_number_is_divisible_by_5() {
		String result = fizzBuzzTest.fizzBuzz(5);
		Assert.assertEquals("Buzz", result);
	}
	@Test
	public void return_fizzbuzz_if_number_is_divisible_by_3_and_5() {
		String result = fizzBuzzTest.fizzBuzz(15);
		Assert.assertEquals("FizzBuzz", result);
	}
	@Test
	public void return_nothing_if_number_is_over_100() {
		String result = fizzBuzzTest.fizzBuzz(175);
		Assert.assertEquals("", result);
	}
	@Test
	public void return_string_if_number_doesnt_meet_requierments() {
		String result = fizzBuzzTest.fizzBuzz(98);
		Assert.assertEquals("98", result);
	}
	@Test
	public void return_if_number_is_a_negative_number() {
		String result = fizzBuzzTest.fizzBuzz(-13);
		Assert.assertEquals("", result);
	}
}

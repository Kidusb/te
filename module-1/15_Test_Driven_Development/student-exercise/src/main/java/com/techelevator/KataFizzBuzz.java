package com.techelevator;

public class KataFizzBuzz {

	
	/**
	 * Classifies a number into Fizz, or Buzz
	 * 
	 * @param Fizz - if number is divisible by 3 
	 * @param Buzz - if number is divisible by 5
	 * @param FizzBuzz - if number is divisible by 3 and 5
	 * @param all other numbers between 1 and 100 (inclusive), convert the number to a string
	 * @param not between 0-100 return empty string
	 * @return string 
	 */

	public KataFizzBuzz() {
		
	}			

	public String fizzBuzz(int divisibleNumber) {
		
		String result = "";

		if (divisibleNumber <= 0 || divisibleNumber >=101) {
			return result;
		}else
		if (divisibleNumber % 3 == 0 && divisibleNumber % 5 == 0) {
			result = "FizzBuzz";
			return result;
		} else 
			if(divisibleNumber % 5 == 0) {
				result = "Buzz";
				return result;
			} else 
				if (divisibleNumber % 3 == 0) {
			result = "Fizz";
			return result;
				} else 
			return Integer.toString(divisibleNumber);	
		
	}

}

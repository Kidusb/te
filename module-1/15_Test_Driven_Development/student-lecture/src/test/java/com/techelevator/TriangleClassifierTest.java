package com.techelevator;

import org.junit.*;

public class TriangleClassifierTest {
	
	private TriangleClassifier classifier; 
	
	@Before 
	public void setup() {
		classifier = new TriangleClassifier();
		
	}
	
	@Test
	public void identify_equilateral_triangle() {
		String type = classifier.classify(4, 4, 4);
		Assert.assertEquals("equilateral", type);
	}
	@Test
	public void identify_scalene_triangle() {
		String type = classifier.classify(2, 3, 4);
		Assert.assertEquals("scalene", type);
	}
	@Test
	public void identify_isosolies_triangle() {
			// testing side1 = side2 but not side3
			// side1= side3 but not side2
			// side 3= side 2 but not equal to side1
		
		//a way to do multiple test on 1
		String type = classifier.classify(2, 2, 3);
		Assert.assertEquals("side 1 and side 2 equals","isosolies", type);
		
		type = classifier.classify(2, 3, 2);
		Assert.assertEquals("side 1 and side 3 equals", "isosolies", type);
	
		type = classifier.classify(3, 2, 2);
		Assert.assertEquals("side 2 and side 3 equals","isosolies", type);
	}
	@Test 
	public void identify_invalid_triangle () {
		//1 + 2 <= 3 || 2+3 <= 1 || 1+3 <= 2
		String type = classifier.classify(1, 4, 6);
		Assert.assertEquals("1+2 <= 3", "invalid", type);
		
		type = classifier.classify(4, 1, 6);
		Assert.assertEquals("2+3 <= 1", "invalid", type);
		
		type = classifier.classify(6, 1, 4);
		Assert.assertEquals("3+2 <= 1", "invalid", type);
		
		type = classifier.classify(0, 2, 3);
		Assert.assertEquals("side1 = 0", "invalid", type);
		
		type = classifier.classify(2, 0, 3);
		Assert.assertEquals("side2 = 0", "invalid", type);
		
		type = classifier.classify(2, 3, 0);
		Assert.assertEquals("side3 = 0", "invalid", type);

		type = classifier.classify(-1, 2, 3);
		Assert.assertEquals("side1 = negative", "invalid", type);
		
		type = classifier.classify(1, -2, 3);
		Assert.assertEquals("side2 = negative", "invalid", type);

		type = classifier.classify(1, 2, -3);
		Assert.assertEquals("side3 = negative", "invalid", type);

		}
	
}


package com.techelevator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

public class FizzWriter {

	/**
	//Create a program to write out the results of FizzBuzz 1 - 300 to file FizzBuzz.txt
	//if number is divisible by 3 or contains 3 print "Fizz"
	//if ^		^	^		^	5 ^		^	 5 print "Buzz"
	//if divisible by 3 and 5 print FizzBuzz
	// otherwise print numbers
	//
	// expected output: FizzBuzz.txt has been created.
	 * new file each time the application runs
	 * app should run and terminate, no user interface
	 * 
	 * 
	 */
	
	private final static String FILE_NAME = "FizzBuzz.txt";
	private final static int UPPER_LIMIT = 300;
	
	public static void main(String[] args) throws IOException {

		File textFile = new File(FILE_NAME);
		
		try( PrintWriter w = new PrintWriter(textFile);
				BufferedWriter buffered = new BufferedWriter(w) ) {
					for (int i = 1; i < UPPER_LIMIT; i++) {
						w.write(getOutput(i) + System.getProperty("line.separator"));
					}
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			for (int i = 1 ; i < UPPER_LIMIT ; i++) {
				System.out.println(getOutput(i));
			}
			
			System.out.println("FizzBuzz.txt has been created!");
		}
		
		
		private static String getOutput(int i) {
			
			String output = String.valueOf(i);
			
			if (i % 3 == 0 || i == 3) {
				output = "Fizz";
			} else if (i % 5 == 0 || i == 5) {
				output = "Buzz";
			} else if (i % 5 == 0 && i % 3 == 0) {
				output = "FizzBuzz";
			}
			
			return output;
		}
		
	}

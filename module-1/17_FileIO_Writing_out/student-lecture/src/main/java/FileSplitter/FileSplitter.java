package FileSplitter;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileSplitter {
	
	public void split(File inputFile, int lineCount) throws FileNotFoundException, SegmentWriterException {
		
		//Split file into parts
		
		SegmentWriter writer = new FileSegmentWriter(inputFile.getAbsolutePath());
		
		try( Scanner fileScanner = new Scanner(inputFile) ) {
			
			int segment = 0;
					
			//wrappin entire loop
			while (fileScanner.hasNextLine() ) {
			
			List<String> lines = new ArrayList<String>();
			
			int count = lineCount; 
					//so we dont change orginal line count back, so it can rest to original value
					while( count > 0 && fileScanner.hasNextLine() ) {
						//while theres something left on the file get next line
						lines.add(fileScanner.nextLine());
						count--;
					}
//					System.out.println(lines);
					
					
					//write the parts of new files
					segment++;
//					writer
					writer.write(lines, segment);
					//created a new interface for this, segmentwriter
					//lines = new ArrayList<String>();
			}
		}
		
		
	}

}

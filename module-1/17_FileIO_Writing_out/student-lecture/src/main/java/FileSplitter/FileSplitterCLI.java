package FileSplitter;

import java.io.File;
import java.io.FileNotFoundException;

import com.techelevator.FileSplitter.view.menu;

public class FileSplitterCLI {

	private menu menu = new menu();
	//		the method we need 
	public void run() {
		
		File file = getFile();
		
//		Workflow ; 
//		get flie from use, 
//		get line count for the split, 
		int lineCount = menu.getLineCountFromUser();
//		split the file into segmants 
		FileSplitter splitter = new FileSplitter();
	try {
		
		splitter.split(file, lineCount);
	} catch (FileNotFoundException e) {
		//TODO handle file not found
		menu.displayMessage("File not found: " + e.getMessage() );
	} catch (SegmentWriterException e) {
		menu.displayMessage("Unable to write file segment: " + e.getMessage() );

	}
//		inform user its complete
	menu.displayMessage("Split Complete");
	//TODO inform user
		
		
	}
	// we need a menu from user, placed up here so we can handle things that are not files first
	private File getFile() {
		File file = null;
		
		while(file == null) {
		//way to ask user , create menu file
		String filename = menu.getPathFromUser();
		file = new File(filename);
		
		if(!file.exists() || !file.isFile() ) {
			menu.displayMessage(filename + "is not valid. Please try again!");
			file = null;
			}
		}
		return file;
	}
	
	public static void main(String[] args) {
			//with static mains we should create AN OBJECT and get out of static as soon as we can
		
		//instance so we can use run
		FileSplitterCLI cli = new FileSplitterCLI();
		cli.run();
	}
	
	// we need a menu from user

}

package FileSplitter;

import java.util.List;

public interface SegmentWriter {

	void write(List<String> lines, int segment) throws SegmentWriterException;
	
	//interface has to include entire interface signitures
	//create class for this action
	
}

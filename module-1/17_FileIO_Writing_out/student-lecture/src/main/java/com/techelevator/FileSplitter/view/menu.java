package com.techelevator.FileSplitter.view;

//first import scanner
import java.util.Scanner;

public class menu {

	private Scanner in = new Scanner(System.in);
	
	public String getPathFromUser() {
		System.out.println("Path to file >>>");
		return in.nextLine();
	}
	
	public void displayMessage(String message) {
		System.out.println(message);
		System.out.flush();
	}
	public int getLineCountFromUser() {
		System.out.println("Line to split >>>");
		int lineCount = in.nextInt();
		in.nextLine();
		return lineCount;
	}
}


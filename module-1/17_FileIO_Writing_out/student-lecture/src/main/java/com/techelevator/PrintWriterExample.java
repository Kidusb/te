package com.techelevator;

import java.io.*;

public class PrintWriterExample {

	public static void main(String[] args) {
		
		//Writing a file without buffer
		//always start by creating new file instance 
		
		// 1. file object with the file path
		File f = new File("test");
		 
		//2.  print writer
		try (PrintWriter writer = new PrintWriter(f)) {
			//this is where we override the print file and everything under this is added to the file, erases old content when doing this
		
			//3. Call the print method with text
			writer.println("This is some data in a file");
			
			
		}catch (FileNotFoundException e) {
			//creating catch for exceptions
			System.out.println("File not found! " + f.getAbsolutePath() );
		}
		
		//Writing to file with buffer
		File bufferedFile = new File("bufferedtest");
		try (PrintWriter writer = new PrintWriter(bufferedFile)) {
			BufferedWriter buffered = new BufferedWriter(writer);
			buffered.write("message\n");
			buffered.write("message\n");

			//writer needs a space added so add "\n" at end to create space
		
		} catch (FileNotFoundException e) {
			System.out.println("File not found! " + bufferedFile.getAbsolutePath());
		} catch (IOException e1) {
			System.out.println("Unknown IOException: " + e1.getMessage() );
		}
		
	}

}

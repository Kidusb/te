package com.techelevator;

import java.util.Scanner;

public class DecimalToBinary {

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);		

		System.out.print("Please enter in a series of decimal values (separated by spaces): ");
		
		
		String enteredValue = in.nextLine();
		String[] sepValue = enteredValue.split(" ");
		
		
		for(int i = 0;  i < sepValue.length ; i++) {
		int inputNumber = Integer.parseInt(sepValue[i]);
		System.out.print(inputNumber + " in binary is ");
		
		decimalToBinary(inputNumber);
		System.out.println(" ");
		}				     										
	}	
	public static void decimalToBinary(int inputNum){
		
		int binaryNum[] = new int[40];
		int index = 0;
		
		while (inputNum >0){
			
			binaryNum[index ++] = inputNum % 2 ;
			inputNum = inputNum / 2;						
		}
		for(int i = index-1;i >= 0;i--){
		       System.out.print(binaryNum[i]);
	
	}		
	}

	}



package com.techelevator;

import java.util.Scanner;

public class Fibonacci {

	public static void main(String[] args) {
		int primary = 0;
		int addNumber = 1;
		int stayNumber = 0;
		
		Scanner in = new Scanner(System.in);
		
		System.out.print("Please enter an integer: ");
		
		int topNumber = in.nextInt();
		in.nextLine();
		
		System.out.print(0 + " ");
		
		for (int nextNum = 1; nextNum <= topNumber;) {
			
			System.out.print(nextNum + " ");
			
			stayNumber = primary;
			primary = nextNum;
			nextNum= nextNum + stayNumber;
		}
	}
}


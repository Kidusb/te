package com.techelevator;

import java.util.Scanner;

public class LinearConvert {

	public static void main(String[] args) {
		
//		String [] choices = {"(m)eter", "(f)eet" };
		Scanner in = new Scanner(System.in);
		
//		Please enter the length: 58
//		Is the measurement in (m)eter, or (f)eet? f
//		58f is 17m.

		System.out.println("Please enter the length: ");
		double length = in.nextDouble();
		in.nextLine();
		System.out.println("Is the measurement in (m)eter, or (f)eet? ");
		String Measur = in.nextLine();
		
		System.out.println((length + Measur) + " is " + doConversion(length, Measur) + (Measur.toUpperCase().startsWith("m") ? "f" : "m"));
	}
	
	public static double doConversion(double length, String Measur) {
		
		
		if(Measur.toLowerCase().startsWith("m")) {
			double feets1 = length * 3.2808399;
			return feets1;
		} else {
			double meters1 = length * 0.3048;
			return meters1;

		}
	}
	}



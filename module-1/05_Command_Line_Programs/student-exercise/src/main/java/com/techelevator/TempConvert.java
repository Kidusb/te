package com.techelevator;

import java.util.Scanner;

public class TempConvert {

	public static void main(String[] args) {
//		String [] choices = {"(C)elcius", "(F)areheit" };
		Scanner in = new Scanner(System.in);
		
		//prompt user to enter a temp and if its C or F
		// convert the temp into opposit so f - c and c - f
		// display old and new temp
			//		"Please enter the temperature: 58
			//			Is the temperature in (C)elcius, or (F)arenheit? F
			//				58F is 14C."
		System.out.println("Please enter the temperature: ");
		int Temp = in.nextInt();
		in.nextLine();
		
		System.out.println("Is the temperature in (C)elcius, or (F)aremheit? ");
		String Degree = in.nextLine();
		
		System.out.println((Temp + Degree) + " is " + doConversion(Temp, Degree) + (Degree.toUpperCase().startsWith("F") ? "C" : "F"));
	}
	
	public static int doConversion(int temp, String Degree) {
		
		
		if(Degree.toUpperCase().startsWith("F")) {
			int TempC = (int)(( temp - 32) / 1.8);
			return TempC;
		} else {
			int TempF = ((int)(temp * 1.8 + 32));
			return TempF;
		}
	}

}

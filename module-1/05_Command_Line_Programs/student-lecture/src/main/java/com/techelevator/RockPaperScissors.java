package com.techelevator;

import java.util.Scanner;

public class RockPaperScissors {

	/*
	 * Command Line programs follow this structure:  Take Input, Calculate Data, Give Output
	 * 
	 * How could we use this structure, with what we know of Java, so far, to write a simple game
	 * like Rock, Paper, Scissors?
	 */
//	requirments 
//	get user input
//	computer choice
//	display choice
//	determain winner
	public static void main(String[] args) {
		String [] choices = {"Rock ", "Paper ", "Scissors "};
		
	 Scanner in = new Scanner(System.in );
	 
		int computerWins = 0;
				int playerWins = 0;
	  // make it run forever)
	 while (true) {
	 System.out.println("Select (1) Rock, (2) Paper, (3) Scissors >>>");
			int userChoice = in.nextInt();
			in.nextLine();
			
			if (userChoice == 0) {
				break;
			}
			
			if (userChoice > 3 || userChoice < 0) {
				System.out.println("Invalid number");
				continue;
	 }
			//look up how to get random numbers, many diff ways to do this
			int computerChoice = (int) (Math.random() *3) + 1;
			
			System.out.println("You choose " + choices[userChoice - 1] + "The Computer choose " + choices[computerChoice - 1]);
			if (userChoice == computerChoice) {
				//tie
				System.out.println("Tie!");
			} else if ( (userChoice == 1 && computerChoice ==3) || 
						(userChoice ==2 && computerChoice ==1) ||
						(userChoice ==3 && computerChoice ==1)) {
				System.out.println("You Win... Congratulations!"); playerWins++;
			} else {
				System.out.println("The Computer Won!"); computerWins++;
			}
			if (userChoice > 3 || userChoice < 0) {
				System.out.println("Invalid number");
				continue;
	 }
			
			System.out.println("Total player wins: " + playerWins);
			System.out.println("Total computer wins: " + computerWins);
			System.out.println("Thank you for playing!");
		
			
			// now make it display rock paper scissors instead of computer choose 1, 2, 3..
			// String [] choice = {"Rock", "Paper", "Scissors"}; added up top
			// then changed above like system.out line from System.out.println("The Computer choose " + computerChoice); to 
			
	}
	}
}

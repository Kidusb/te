package com.techelevator;

import java.util.Scanner;
import com.techelevator.data.NumberCruncher;

import java.util.*;

public class ImportExample {
	
	public static void main(String[] args) {
		
		java.util.Scanner in = new Scanner(java.lang.System.in);
		
		java.lang.System.out.println("Pick a whole number?");
		
		String userChoice = in.nextLine();
		int number = Integer.parseInt(userChoice);
		
		com.techelevator.data.NumberCruncher numberCruncher = new com.techelevator.data.NumberCruncher();
		java.lang.Integer newNumber = numberCruncher.multiplyNumberBy10(number);
		
		System.out.println("Your number multiplied by 10 is " + newNumber);

	}

}

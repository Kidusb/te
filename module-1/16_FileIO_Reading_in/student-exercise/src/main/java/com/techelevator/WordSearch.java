package com.techelevator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class WordSearch {

	public static void main(String[] args) {

		//DISCRIPTION
		// program: asks user for a search string and file system path
		//program searches file for occurrences of the search string
		//each time it finds line that matches,
		//it displays line number and contents of line it was found in

		//alices_adventures_in_wonderland.txt
		//What is the file that should be searched?
		//What is the search word you are looking for?


		//		userInput = UserInput();
		//		searchedWord = SearchedWord();

		try (Scanner in = new Scanner(System.in)) {

			System.out.println("What is the file that should be searched? ");

			String searchname = in.nextLine();

			System.out.println("What is the search word you are looking for? ");
			String searchword = in.nextLine();

			System.out.println("Do you want the search to be case sensative? (y or n) ");
			String answers = in.nextLine();

			boolean isItCaseSensative = false;

			if (answers.equalsIgnoreCase("y")) {
				isItCaseSensative = true;
			}

			try {	 
				File file = getSearchFile(searchword);
				try(Scanner searchfile = new Scanner(file)) {
					int lineNum = 1;
					while (searchfile.hasNextLine()) {
						String line = searchfile.nextLine();
						if (isItCaseSensative) {
							if(line.contains(searchname)) {
								System.out.println(lineNum + ") " + line);
							}
						} else {
							if (line.toUpperCase().contains(searchname.toUpperCase() ) ) {
								System.out.println(lineNum + ") " + line);
							}

						}
						lineNum++;
					}
				}
			} catch (FileNotFoundException e) {
				System.out.println(e.getMessage());
				System.out.println(1);
			} catch (IOException e) {
				System.out.println(e.getMessage());
				System.out.println(1);
			}	
		}

	}

	private static File getSearchFile(String searchname) throws IOException {
		File file = new File(searchname);
		if(!file.exists()) {
			throw new FileNotFoundException ("Search file doesnt exist");
		}
		if(file.isFile()) {
			throw new IOException ("Search file is not a file");
		}
		return file;
	}
}


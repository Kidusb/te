package FileReaderMainRefactored;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class FileReaderMain {
	
	public static void main(String[] args) {
		
		Menu menu= new Menu();
		
		// get the file from user
		File file = menu.getFileFromUser();
		// get the line count from user
		int lineCount = menu.getLineCountFromUser();
		// reader the file
		FileReader reader = new TextFileReader(file);
		List<String> lines = new ArrayList<String>();
		
		try { 
			lines = reader.read(lineCount);
		}catch (FileNotFoundException e) {
			menu.DisplayMessage("File not found: " + file.getAbsolutePath());
			
		}catch (Exception e) {
			menu.DisplayMessage("Unknown erroe occured: " + e.getMessage());
		}
		
		lines= reader.read(lineCount);
		//Display the lines to user
		for (String line : lines) {
			menu.DisplayMessage(line);
		}
	}

}

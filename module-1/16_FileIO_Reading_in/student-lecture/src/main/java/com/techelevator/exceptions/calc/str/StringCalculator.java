package com.techelevator.exceptions.calc.str;

import java.util.ArrayList;
import java.util.List;

import com.techelevator.exceptions.calc.Calculator;

public class StringCalculator implements Calculator {

	
	@Override
	public List<String> calculate(List<String> values) throws InvalidStringException {
		//adding throws gives error becuaes it needed to be added to the exceptions
		
		StringReverser reverser = new StringReverser();
		List<String> results = new ArrayList<String>();
		
		for (int i = 0; i < values.size(); i++) {
			
			//if statament to add new exception
			if (values.get(i) != null && values.get(i).equalsIgnoreCase("bob")) {
				throw new InvalidStringException("We dont allow", values.get(i));
				//this gives error, invalid string exception..we can add throws or add a try/catch statement
			}
			String reversed = null;
			
			try {
			//we decide to place try here, the code that has the risk around it
			reversed = reverser.reverseString(values.get(i));
			} catch (NullPointerException e) {
				reversed = "null";
				
			} finally {
			results.add(reversed);
		}
		}
		 
		return results;
	}

	
}


package com.techelevator.exceptions.calc.str;

import java.util.List;

import com.techelevator.exceptions.calc.Calculator;
import com.techelevator.exceptions.calc.Menu;

public class MainStringCalc {

	public static void main(String[] args) {
		
		Menu menu = new Menu();
		Calculator strCalc = new StringCalculator();
		
		List<String> values = menu.getValuesFromUser();

		try {
		List<String> results = strCalc.calculate(values);	
		
		//found a null exception error
		// we pick the level we want to handle the error in
		//
		
		for (String result : results) {
			menu.displayUserMessage(result);
		} 
		} catch (NullPointerException ex) {
			System.out.println("The list is null");
		} catch (InvalidStringException e) {
			System.out.println(e.getMessage() + " " +e.getOriginalValue() );

		}
		
		}
	

	}



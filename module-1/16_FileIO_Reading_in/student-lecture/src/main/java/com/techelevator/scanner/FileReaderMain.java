package com.techelevator.scanner;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FileReaderMain {

	public static void main(String[] args) {

		//open a file ask if its a file and number of lines
		//read and print lines
		
		//first thing we need is input from user so scanner in
		Scanner in = new Scanner(System.in);
			
		System.out.print("Path to file >>>");
		String filename = in.nextLine();
		//we are pringing a text and the users next line is the answer and variable filename
		File file = new File(filename);
		//have to import io.file for this to work
		System.out.print("Number of lines >>>");
		int lineCounts = in.nextInt();
		in.nextLine();
		
		try (Scanner fileScanner = new Scanner(file) ) {
			//create a while statement to go thru files
			while ( fileScanner.hasNextLine() && lineCounts > 0 ) {
				String line = fileScanner.nextLine();
					System.out.println(line);
					lineCounts--;
			} 
			//above is asking if there is another line if so it will print text
		}catch (FileNotFoundException e) {
			//had to import filenotfoundexception
			System.out.println("File " + file.getAbsolutePath() + " not found");
		}
	}

}

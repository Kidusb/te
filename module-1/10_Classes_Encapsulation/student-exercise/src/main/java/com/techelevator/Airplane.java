package com.techelevator;

public class Airplane {
	
	public String planeNumber;
	public int bookedFirstClassSeats;
	public int totalFirstClassSeats = 0;
	public int bookedCoachSeats;
	public int totalCoachSeats = 0;

	public Airplane(String planeNumber, int totalFirstClassSeats, int totalCoachSeats) {
		this.planeNumber = planeNumber;
		this.totalFirstClassSeats = totalFirstClassSeats;
		this.totalCoachSeats = totalCoachSeats;
	}
	
	public int availableFirstClassSeats() {
		return bookedFirstClassSeats - totalFirstClassSeats;
	}
	public int availableCoachSeats() {
		return totalCoachSeats - bookedCoachSeats;
	}

	public String getPlaneNumber() {
		return planeNumber;
	}

	public int getBookedFirstClassSeats() {
		return bookedFirstClassSeats;
	}

	public int getTotalFirstClassSeats() {
		return totalFirstClassSeats;
	}

	public int getBookedCoachSeats() {
		return bookedCoachSeats;
	}

	public int getTotalCoachSeats() {
		return totalCoachSeats;
	}
	
	public int getAvailableFirstClassSeats() {
		return bookedFirstClassSeats - totalFirstClassSeats;
	}
	public int getAvailableCoachSeats() {
		return totalCoachSeats - bookedCoachSeats;
	}
	
	public boolean reserveSeats(boolean firstClass, int totalNumberOfSeats) {
		if(firstClass) {
//				if (getAvailableFirstClassSeats() >= totalNumberOfSeats) {
//					bookedFirstClassSeats += totalNumberOfSeats;
//					return true;
//				} else {
//					if (getAvailableCoachSeats() >= totalNumberOfSeats) {
//						bookedCoachSeats += totalNumberOfSeats;
//						return true;
//					}
//				}
//				return false;
//		}
//	}
//}
			
		if (totalNumberOfSeats > getAvailableFirstClassSeats() ) {
				return false;
			}
			bookedFirstClassSeats += totalNumberOfSeats;
		} else {
			if (totalNumberOfSeats > getAvailableCoachSeats() ) {
				return false;
			}
			bookedCoachSeats += totalNumberOfSeats;
		}
		return true;
	}

	}




package com.techelevator;

public class Employee {
	public int employeeId;
	public String firstName;
	public String lastName;
//	public String fullName;
	public String department;
	public double annualSalary;
	
	public Employee(int employeeId, String firstName, String lastName, double salary) {
		this.annualSalary = salary;
		this.employeeId = employeeId;
		this.firstName = firstName;
		this.lastName = lastName;
		
	}
	
	public String getFullName() {
		return lastName +", " + firstName;
	}
	public void raiseSalary(double percent) {
//		return (salary/100) * percent;
		annualSalary = (annualSalary/100) * percent + annualSalary;
	}

		public String getLastName() {
			return lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		public String getDepartment() {
			return department;
		}
		public void setDepartment(String department) {
			this.department = department;
		}
		public int getEmployeeId() {
			return employeeId;
		}
		public String getFirstName() {
			return firstName;
		}
		public double getAnnualSalary() {
			return annualSalary;
			
		}
	}
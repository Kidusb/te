package com.techelevator;

public class FruitTree {


		public String typeOfFruit;
		public int piecesOfFruitLeft;
		
		public FruitTree(String typeOfFruit, int startingPiecesOfFruit) {
			this.piecesOfFruitLeft = startingPiecesOfFruit;
			this.typeOfFruit = typeOfFruit;
			
		}
			public String getTypeOfFruit() {
			return typeOfFruit;
		}
		public int getPiecesOfFruitLeft() {
			return piecesOfFruitLeft;
		}
		public boolean pickFruit(int numberOfPiecesToRemove) {
			if (piecesOfFruitLeft - numberOfPiecesToRemove >= 0) {
				piecesOfFruitLeft -= numberOfPiecesToRemove;
				return true;
			} else {
				return false;
			}
		}
	
}

	
//	public String getTypeOfFruit() {
//		return typeOfFruit;
//	}
//	public int getPiecesOfFruitLeft() {
//		return piecesOfFruitLeft;
//	}
//	
//	public boolean pickFruit(int numberOfPiecesToRemove) {
//		if (pickFruit(0)) {
//			return false;
//		} else {
//			return true;
//		}
//		
//	}


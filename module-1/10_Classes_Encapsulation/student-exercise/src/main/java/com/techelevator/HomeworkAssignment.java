package com.techelevator;

public class HomeworkAssignment {

	public String typeOfFruit;
	public int piecesOfFruitLeft;
	
	public String getTypeOfFruit() {
		return typeOfFruit;
	}
	public int getPiecesOfFruitLeft() {
		return piecesOfFruitLeft;
	}
	
}

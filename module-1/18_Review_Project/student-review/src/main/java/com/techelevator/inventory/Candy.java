package com.techelevator.inventory;

public class Candy extends Item {
	public Candy(String name, double price) {
	super(name, price, "Munch Munch, Yum!");
	//extend item class and hard code things we can like sound
	// name and price can change for these things so not hard coded
	}
}

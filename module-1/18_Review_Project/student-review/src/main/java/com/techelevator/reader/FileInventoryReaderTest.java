package com.techelevator.reader;

import org.junit.Assert;

public class FileInventoryReaderTest {
	
	private InentoryReader reader;
	private static final String INVENTORY_FILE = "vendingmachine.csv";
	// final and static make it perminate or closest thing to perminate
	
	@Before
	public void setup() {
		reader = new FileInventoryReader(INVENTORY_FILE);
		
	}

	@Test
	public void inventory_has_correct_number_of_items() {
		int expectedItemCount = 16;
		
		Map<String, Slot> inventory = null;
		try { 
				inventory = reader.load();
		} catch (FileNotFoundException 3) {
			Assert.fail("Inventory File not Found");
		}
		
		Assert.assertNotNull(inventory);
		Assert.assertEquals(expectedItemCount, inventory.size() );
				
		Assert.assertEquals("Potato Chips", inventory.get("A1").getItem().getName() );
		Assert.assertEquals("Triplemint", inventory.get("D4").getItem().getName() );
	}
}


package com.techelevator.reader;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.techelevator.inventory.Slot;

public class FileInventoryReader implements InventoryReader{
	
	private String filename;
	
	public FileInventoryReader(String filename) {
		this.filename = filename;
	}

	@Override 
	public Map<String, Slot> load() {
		
		//get lines from file
		List<String> lines = readAllLinesFromInventory();
//		split each line into parts
//		build a slot
//		add the key and the slot to map
//		return map
		
		return null;
	}
	
	private Map<String, Slot> createInventoryMapFromLines(List<String> lines) {
		Map<String, Slot> inventoryMap = new HashMap<String, Slot> ();
		// main thing we want to know is if there are 16 entries 
		for (String line : lines) {
			String[] parts = line.split("\\|");
			
			Slot slot = new Slot(new Item("", 0));
			inventoryMap.put(line, slot);
		}
		return inventoryMap;
	}
	private Slot createSlotFromParts(String[] parts) {
		//create new slot
		Slot newSlot = null;
		//gets name from line parts
		String name = parts[1];
		// gets the price 
		double price = Double.parseDouble(parts[2]) ;
		// get type 
		String type = parts[3];
		
		Item item = null;
		
		if (type.equalsIgnoreCase("Chip") ) {
			// if type is chip creat new item of type chips
			item = new Chips(name, price);
		}
		if (type.equalsIgnoreCase("Candy") ) {
			item = new Candy(name, price);
		}
		if (type.equalsIgnoreCase("Drink") ) {
			item = new Drink(name, price);
		}
			newSlot = new Slot (new Chips(name, price);
			newSlot = new Slot (item);
		}
	return new Slot (item);
	}
	
	//this methods only job is get a list to hold each line of the file
	private List<String> readAllLinesFromInventory() throws FileNotFoundException {
		List<String> lines = new ArrayList<String>();

	//a file object for file
		File inventoryFile = new File(filename);
		
		//create a scanner with the file object
		try(Scanner fileScanner = new Scanner(inventoryFile) ) {
			//loop while file has more lines of text
			while (fileScanner.hasNextLine()  ) {
			//read next line
				String line = fileScanner.nextLine();
				//add the line to list
				lines.add(line);
			}
		}
		//ability to get and clean up file
	}
	
	return lines;

}

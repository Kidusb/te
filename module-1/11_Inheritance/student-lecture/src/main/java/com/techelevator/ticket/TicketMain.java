package com.techelevator.ticket;

public class TicketMain {

	public static void main (String[] args) {
		
		Ticket regularTicket = new Ticket(10);
		
		System.out.println("Price: " + regularTicket.getPrice());
		System.out.println("Fee: " + regularTicket.getFee());
		System.out.println("Total: " + regularTicket.getTotalPrice());
		
		WillCallTicket willCallTicket = new WillCallTicket (10, "Joe"); 
		System.out.println("Price: " + willCallTicket.getPrice());
		System.out.println("Fee: " + willCallTicket.getFee());
		System.out.println("Total: " + willCallTicket.getTotalPrice());
	}
}
